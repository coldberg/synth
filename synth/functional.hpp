#pragma once

#include <cmath>
#include <functional>

namespace functional {

    template <typename _Xtype>
    static auto pi (_Xtype s_scale = _Xtype (1)) {
        return s_scale * _Xtype (3.1415926535897932);
    }

    template <typename _Xtype>
    static auto make_square () {
        return [] (_Xtype s_time) { 
            return std::copysign (_Xtype (1), std::sin (s_time)); 
        };
    }

    template <typename _Xtype>
    static auto make_sawtooth () {
        return [s_inv_pi = _Xtype(1)/pi<_Xtype> (2)] (_Xtype s_time) { 
            return _Xtype (1) - _Xtype (2) * std::fmod (s_time * s_inv_pi, _Xtype (1));
        };
    }

    template <typename _Xtype>
    static auto make_triangle () {
        return [s_base0 = make_square<_Xtype> (),
                s_base1 = make_sawtooth<_Xtype> (),
                s_offs = pi<_Xtype> (_Xtype (0.5))]
        (_Xtype s_time) { 
            s_time += s_offs;
            return _Xtype(1) - _Xtype (2) * s_base1 (s_time) * s_base0 (s_time);
        };
    }

    template <typename _Ytype, typename _Xtype = _Ytype>
    static auto select_overload (_Ytype (&s_fun) (_Xtype)) {
        return [&s_fun] (_Xtype x) -> _Ytype {
            return s_fun (x);
        };
    }

    template <typename _Ytype, typename _Xtype = _Ytype, typename _Function>
    static auto make_cyclic (_Function&& s_fun, _Xtype s_frequency, _Xtype s_phase = _Xtype (0), _Ytype s_yscale = _Ytype (1), _Xtype s_xscale = pi<_Xtype> (2)) {
        return [s_fun = std::forward<_Function> (s_fun), s_frequency, s_phase, s_yscale, s_xscale] (_Xtype s_time) mutable->_Ytype {
            return s_yscale * s_fun (s_xscale * (s_frequency * s_time + s_phase));
        };
    }

}
