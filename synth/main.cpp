#include <iostream>
#include <fstream>
#include <cstdlib>
#include "functional.hpp"
#include "plot.hpp"

#define SDL_MAIN_HANDLED
#include <SDL.h>

void audio_callback (void* s_userdata, float* s_stream, std::int32_t s_length) {
    std::cout << "bbbzzz bzzz brrrtzz...\n";
}


int main () {
    SDL_Init (SDL_INIT_EVERYTHING);
    SDL_Window* s_window = SDL_CreateWindow ("", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1280u, 720u, 0u);

    auto s = SDL_GetNumAudioDevices (0);
    for (auto i = 0; i < s; ++i) {
        std::printf ("%s\n", SDL_GetAudioDeviceName (i, 0));
    }

    SDL_AudioSpec s_want = {};
    SDL_AudioSpec s_have = {};
    SDL_AudioDeviceID s_dev;

    s_want.userdata = nullptr;
    s_want    .freq = 48000;
    s_want  .format = AUDIO_F32;
    s_want.channels = 2;
    s_want .samples = 4096;
    s_want.callback = (SDL_AudioCallback)audio_callback;
    s_dev = SDL_OpenAudioDevice (nullptr, 0, &s_want, &s_have, SDL_AUDIO_ALLOW_FORMAT_CHANGE);

    SDL_PauseAudioDevice (s_dev, 0);

    SDL_Event s_event;
    while (!SDL_QuitRequested ()) {
        if (SDL_PollEvent (&s_event)) {
            continue;
        }        
    }

    SDL_CloseAudioDevice (s_dev);
    SDL_DestroyWindow (s_window);
    SDL_Quit ();
    return 0;
}