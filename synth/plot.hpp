#pragma once

template <typename _Function, typename _Xtype, typename _Mtype = _Xtype>
inline void plot (_Function&& s_fun, _Xtype s_x0, _Xtype s_x1, _Mtype s_mul) {
    std::ofstream s_out ("plot.csv");
    s_out << "x, y\n";
    for (auto i = s_x0; i <= s_x1; ++i) {
        const auto t = i*s_mul;
        s_out << t << ", " <<  s_fun (t) << "\n";
    }
    s_out.flush ();
    s_out.close ();
    std::system (R"(C:\Devtools\WinPython-64bit-3.5.2.1Qt5\python-3.5.2.amd64\python.exe plot.py plot.csv)");
}

