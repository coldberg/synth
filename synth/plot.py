import matplotlib.pyplot as plt 
import numpy as np;
import sys;

data = np.genfromtxt (sys.argv [1], delimiter=',', names=True)
# print (data)
plt.plot (data['x'], data['y'], '-')
plt.show ()